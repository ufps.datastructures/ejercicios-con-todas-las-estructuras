/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 *
 * @author docente
 */
public class Pila<T extends Comparable> {

    //decorator:
    private ListaCD<T> tope = new ListaCD();

    public Pila() {
    }

    public void push(T info) {
        this.tope.insertarInicio(info);
    }

    public T pop() {
        return this.tope.remove(0);
    }

    public boolean isVacia() {
        return this.tope.isVacia();
    }

    public int size() {
        return this.tope.getSize();
    }
}
