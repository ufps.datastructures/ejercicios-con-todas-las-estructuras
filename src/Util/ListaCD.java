/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

import java.util.Iterator;

/**
 *
 * @author docente
 */
public class ListaCD<T extends Comparable> implements Iterable<T>{

    private NodoD<T> centinela;
    private int size = 0;

    public ListaCD() {
        this.centinela = new NodoD();
        this.centinela.setAnt(centinela);
        this.centinela.setSig(centinela);
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.centinela.getSig(), this.centinela);
        this.centinela.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.size++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.centinela, this.centinela.getAnt());
        nuevo.getAnt().setSig(nuevo);
        this.centinela.setAnt(nuevo);
        this.size++;
    }

    public String toString() {
        if (this.isVacia()) {
            return "Lista Vacía";
        }
        String msg = "";
        for (NodoD<T> aux = this.centinela.getSig(); aux != this.centinela; aux = aux.getSig()) {
            msg += aux.getInfo().toString() + "\t";
        }
        return msg;
    }

    public boolean isVacia() {
        //return this.size==0
        //return this.centinela==this.centinela.getAnt();
        return this.centinela == this.centinela.getSig();
    }

    public int getSize() {
        return size;
    }

    public T get(int i){
        return this.getPos(i).getInfo();
    }
    
    public void set(int i, T elemento){
        this.getPos(i).setInfo(elemento);
    }
    
    public T remove(int i) {

        try {
            NodoD<T> borrar = this.getPos(i);
            borrar.getAnt().setSig(borrar.getSig());
            borrar.getSig().setAnt(borrar.getAnt());
            borrarNodo(borrar);
            this.size--;
            return borrar.getInfo();
            
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    private void borrarNodo(NodoD<T> x)
    {
        x.setSig(x); //x.setSig(null)
        x.setAnt(x);//x.setAnt(null)
    }
    
    
    private NodoD<T> getPos(int i) {
        this.validarPos(i);
        //Referenciar, no crear
        NodoD<T> pos = this.centinela.getSig();
        for (int j = 0; j < i; j++) {
            pos = pos.getSig();
        }
        return pos;
    }

    private void validarPos(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("Indice: " + i + " fuera de Rango");
        }
    }
    
    public void sort()
    {
        //:)
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorListaCD(this.centinela);
    }

}
