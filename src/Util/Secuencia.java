/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 * Estructura de datos estática
 *
 * @author Docente
 * @param <T>
 */
public class Secuencia<T extends Comparable> {

    private T[] vector;

    public Secuencia() {
    }

    public Secuencia(T[] vector) {
        this.vector = vector;
    }

    //Crear:
    public Secuencia(int n) {
        if (n <= 0) {
            throw new RuntimeException("Error dato debe ser >0:" + n);
        }
        //Ideal:s
        //this.vector=new T[n];
        Object temporal[] = new Object[n];
        this.vector = (T[]) temporal;
    }

    //Insertar:
    public void add(int i, T elemento) {
        this.validar(i);
        this.vector[i] = elemento;
    }

    public void set(int i, T elemento) {
        this.add(i, elemento);
    }

    public T get(int i) {
        this.validar(i);
        return this.vector[i];
    }

    public int size() {
        return this.vector.length;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.vector.length) {
            throw new RuntimeException("Error dato fuera de rango:" + i);
        }
    }

    public void sort() {

        T ArrayN[] = this.vector;
        /* Bucle desde 0 hasta la longitud del array -1 */
        for (int i = 0; i < ArrayN.length - 1; i++) {
            /* Bucle anidado desde 0 hasta la longitud del array -1 */
            for (int j = 0; j < ArrayN.length - 1; j++) {
                /* Si el número almacenado en la posición j es mayor que el de la posición j+1 (el siguiente del array) */
                int c = ArrayN[j].compareTo(ArrayN[j + 1]);
                if (c > 0) {
                    /* guardamos el número de la posicion j+1 en una variable (el menor) */
                    T temp = ArrayN[j + 1];
                    /* Lo intercambiamos de posición */
                    ArrayN[j + 1] = ArrayN[j];
                    ArrayN[j] = temp;
                    /* y volvemos al inicio para comparar los siguientes hasta que todos se hayan comparado*/
 /* de esta forma vamos dejando los números mayores al final del array en orden*/
                }
            }
        }

    }
    
    
    
    public Secuencia<T> getUnion(Secuencia<T> vector2)
    {
        return null;
    }
    
    public Secuencia<T> getInterseccion(Secuencia<T> vector2)
    {
        return null;
    }
    
    public Secuencia<T> getDiferencia(Secuencia<T> vector2)
    {
        return null;
    }
    
    public Secuencia<T> getDiferenciaAsimetrica(Secuencia<T> vector2)
    {
        return null;
    }
    
    
    public Secuencia<T> []getPares()
    {
        return null;
    }
}
