/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ArchivoLeerURL;

/**
 *
 * @author madarme
 */
public class TestLeerURL_Fraccionario {

    public static void main(String[] args) {
        String url = "https://gitlab.com/madarme/archivos-persistencia/-/raw/master/fraccionarios.csv";
        String url2= "https://docs.google.com/spreadsheets/d/e/2PACX-1vRUcfkj_wVWoDlqochyzKOMuXOBq064NcXJD2wEXO2Vj5DdUIWV_wqpn6Prxkmpa16z-nJobo3yrjAK/pub?gid=0&single=true&output=csv";
        ArchivoLeerURL archivo = new ArchivoLeerURL(url2);
        Object datos[] = archivo.leerArchivo();
        for (Object dato : datos) {
            System.out.println(dato.toString());
        }

    }

}
