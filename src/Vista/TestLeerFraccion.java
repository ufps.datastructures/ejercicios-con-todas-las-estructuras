/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ArchivoLeerURL;

/**
 *
 * @author docente
 */
public class TestLeerFraccion {
    public static void main(String[] args) {
        String url="https://docs.google.com/spreadsheets/d/e/2PACX-1vR42bi1hC0bcgU6IXL0G2bDyb2nj6e80x9E5ns-VTGPcU5hd29ipKVMfWPSTMeM4rkjvw7du6vTYkDN/pub?gid=0&single=true&output=csv";
        //String url="https://madarme.co";
        ArchivoLeerURL archivo=new ArchivoLeerURL(url);
        Object datos[]=archivo.leerArchivo();
        for(Object dato: datos)
        {
            String fila=dato.toString();
            System.out.println(fila);
        }
    }
    
}
