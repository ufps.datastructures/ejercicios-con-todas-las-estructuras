/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Cadena;

/**
 *
 * @author docente
 */
public class TestPalindrome {
    
    public static void main(String[] args) {
        Consola pantalla=new Consola();
        String cadena=pantalla.leerCadena("Digite una cadena:");
        Cadena c=new Cadena(cadena);
        if(c.isPalindrome())
            pantalla.imprimir("SI es palíndrome :)");
        else
            pantalla.imprimir("NO es palíndrome :(");
    }
}
