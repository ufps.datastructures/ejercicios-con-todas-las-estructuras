/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Modelo.Departamento;
import Modelo.Municipio;
import Modelo.NotificacionVacunado;
import Negocio.SistemaNacionalVacunacion;
import Util.ListaCD;
import java.time.LocalDateTime;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class TestSistemaVacunacion {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String urlPer = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/personas.csv";
        String urlDpto = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/departamento.csv";
        String urlMun = "https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/municipios.csv";
        String urlVac = "https://gitlab.com/madarme/archivos-persistencia/-/raw/master/vacuna/vacuna.csv";

        try {
            System.out.println("Digite la etapa de vacunacion: ");
            int etapa = sc.nextInt();

            SistemaNacionalVacunacion s = new SistemaNacionalVacunacion(etapa, urlDpto, urlMun, urlPer, urlVac);

            ListaCD<Municipio> municipios = s.getMunicipiosSinRegistro();
            System.out.println("Municipios sin registro: " + municipios.getSize());
            for (Municipio m : municipios) {
                System.out.println(m.toString());
            }
            
            ListaCD<Departamento> dptos = s.enviarNotificaciones(LocalDateTime.of(2020, 5, 20, 0, 0));
            System.out.println("Los departamentos mas notificados fueron: ");
            int c = 1;
            for(Departamento d : dptos){
                if(d != null) System.out.println("Etapa " + c + ": " + d.getNombreDpto());
                else System.out.println("Etapa " + c +  ": No hubo notificaciones");
                c++;
            }
            
            System.out.println("");
            
            ListaCD<NotificacionVacunado> noti = s.getRegistros();
            for(NotificacionVacunado n : noti){
                System.out.println(n.toString());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
