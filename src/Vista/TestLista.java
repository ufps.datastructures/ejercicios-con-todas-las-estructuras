/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ListaS;

/**
 *
 * @author profesor
 */
public class TestLista {

    public static void main(String[] args) {
        ListaS<Integer> lista1 = new ListaS();
        ListaS<Integer> lista2 = new ListaS();
        for (int i = 0; i < 10; i++) {
            lista1.insertarInicio(i + 1);
        }
        for (int i = 0; i < 10; i++) {
            lista2.insertarFin(i + 1);
        }
        try {
        System.out.println(lista1);
        System.out.println(lista2);
        lista2.set(2, 1000);
        System.out.println(lista2);
        lista2.remove(0);
        System.out.println(lista2);
        lista2.remove(40);
        System.out.println(lista2);
        
            if (lista1.contains(null)) {
                System.out.println("existe");
            } else {
                System.out.println("no existe");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
