/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Semestre;
import Util.ArchivoLeerURL;
import Util.ListaCD;

/**
 *
 * @author profesor
 */
public class SIA {
    
    private Semestre [] semestres = new Semestre[10];
    
    public SIA(){
        
    }
    
    public SIA(String url){
        for(int i = 0; i < 10; i++){
            this.semestres[i] = new Semestre((byte)(i+1));
        }
        this.cargarDatos(url);
    }
    
    private void cargarDatos(String url){
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object [] datos = archivo.leerArchivo();
        for(int i = 1; i < datos.length; i++){
            String fila = datos[i].toString();
            String [] datosEst = fila.split(";");
            int sem = Integer.parseInt(datosEst[0]);
            this.semestres[sem-1].addEstudiante(fila);
        }
    }
    
    public ListaCD<String> getListado(byte id){
        this.validar(id);
        return this.semestres[id-1].getListado();
    }
    
    private void validar(byte id){
        if(id < 1 || id > 10) throw new RuntimeException("Error. Semestre invalido");
    }
    
}
