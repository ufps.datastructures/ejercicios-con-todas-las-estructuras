/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.*;
import Util.ArchivoLeerURL;
import Util.ColaP;
import Util.ListaCD;
import Util.Pila;
import java.time.LocalDateTime;

/**
 *
 * @author madarme
 */
public class SistemaNacionalVacunacion {

    private int etapa;
    private String urlDpto, urlMunicipio, urlPersona, urlContenedor;
    private final ListaCD<Departamento> dptos = new ListaCD();
    private final ListaCD<NotificacionVacunado> registros = new ListaCD();
    private Proveedor[] proveedores;

    public SistemaNacionalVacunacion() {

    }

    public SistemaNacionalVacunacion(int etapa, String urlDpto, String urlMunicipio, String urlPersona, String urlContenedor) throws Exception {
        this.etapa = etapa;
        this.urlDpto = urlDpto;
        this.urlMunicipio = urlMunicipio;
        this.urlPersona = urlPersona;
        this.urlContenedor = urlContenedor;
        this.cargarDepartamentos();
        this.cargarMunicipios();
        this.cargarPersonas();
        this.cargarProveedores();
    }

    private void cargarDepartamentos() throws Exception {
        ArchivoLeerURL a = new ArchivoLeerURL(this.urlDpto);
        Object[] data = a.leerArchivo();
        for (int i = 1; i < data.length; i++) {
            String[] rowData = data[i].toString().split(";");
            int id = Integer.parseInt(rowData[0]);
            String name = rowData[1];
            this.dptos.insertarFin(new Departamento(id, name));
        }
    }

    private void cargarMunicipios() throws Exception {
        ArchivoLeerURL a = new ArchivoLeerURL(this.urlMunicipio);
        Object[] data = a.leerArchivo();
        for (int i = 1; i < data.length; i++) {
            String[] rowData = data[i].toString().split(";");
            int idDpto = Integer.parseInt(rowData[0]);
            int idMun = Integer.parseInt(rowData[1]);
            String name = rowData[2];
            Departamento d = this.getDptoById(idDpto);
            if (d == null) {
                throw new RuntimeException("El departamento no existe");
            }
            d.addMunicipio(idMun, name);
        }
    }

    private void cargarPersonas() throws Exception {
        ArchivoLeerURL a = new ArchivoLeerURL(this.urlPersona);
        Object[] data = a.leerArchivo();
        for (int i = 1; i < data.length; i++) {
            String[] rowData = data[i].toString().split(";");
            long cedula = Long.parseLong(rowData[0]);
            String nombre = rowData[1];
            String[] fecha = rowData[2].split("-");
            LocalDateTime fechaNueva = LocalDateTime.of(Integer.parseInt(fecha[0]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[2]), 0, 0);
            int idMun = Integer.parseInt(rowData[3]);
            String email = rowData[4];
            Municipio m = this.getMunById(idMun);
            if (m == null) {
                throw new RuntimeException("El municipio no existe");
            }
            m.addPersona(cedula, nombre, fechaNueva, email);
        }
    }

    private void cargarProveedores() throws Exception {
        ArchivoLeerURL a = new ArchivoLeerURL(this.urlContenedor);
        Object[] data = a.leerArchivo();
        this.proveedores = new Proveedor[data.length - 1];
        for (int i = 1; i < data.length; i++) {
            String[] rowData = data[i].toString().split(";");
            int idProv = Integer.parseInt(rowData[0]);
            String nombre = rowData[1];
            int dosis = Integer.parseInt(rowData[2]);
            String[] fecha = rowData[3].split("/");
            LocalDateTime fechaVenc = LocalDateTime.of(Integer.parseInt(fecha[2]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[1]), 0, 0);
            Proveedor p = new Proveedor(idProv, nombre);
            this.proveedores[i - 1] = p;
            p.addVacunas(dosis, fechaVenc);
        }
    }

    private Departamento getDptoById(int idDpto) {
        for (Departamento d : this.dptos) {
            if (d.getId_dpto() == idDpto) {
                return d;
            }
        }
        return null;
    }

    private Municipio getMunById(int idMun) {
        for (Departamento d : this.dptos) {
            Municipio m = d.getMunicipio(idMun);
            if (m != null) {
                return m;
            }
        }
        return null;
    }

    public ListaCD<Municipio> getMunicipiosSinRegistro() throws Exception {
        ListaCD<Municipio> municipios = new ListaCD();
        for (Departamento d : this.dptos) {
            for (Municipio m : d.getMunicipios()) {
                if (m.getPersonas().getTamanio() == 0) {
                    municipios.insertarFin(m);
                }
            }
        }
        return municipios;
    }

    public ListaCD<Departamento> enviarNotificaciones(LocalDateTime fechaInicio) throws Exception {
        ListaCD<Departamento> listaDptos = new ListaCD();
        int edadEtapa = 80;
        LocalDateTime fecha;
        for (int i = 1; i <= this.etapa; i++) {
            if (i >= 6) {
                edadEtapa = 15;
            }
            int mayor = 0, contadorDpto = 0;
            Departamento dptoMayor = null;
            for (Departamento d : this.dptos) {
                for (Municipio m : d.getMunicipios()) {
                    if (m.getPersonas().getTamanio() > 0) {
                        contadorDpto += this.vacunarMunicipio(d, m, fechaInicio, edadEtapa);
                    }
                }
                if (contadorDpto > mayor) {
                    mayor = contadorDpto;
                    dptoMayor = d;
                }
            }
            listaDptos.insertarFin(dptoMayor);
            edadEtapa -= 10;
        }
        return listaDptos;
    }

    private int vacunarMunicipio(Departamento d, Municipio m, LocalDateTime fechaInicio, int edadEtapa) throws Exception {
        int notificaciones = 0;
        LocalDateTime fecha = LocalDateTime.of(fechaInicio.getYear(), fechaInicio.getMonth(), fechaInicio.getDayOfMonth(), 6, 0);
        int idProv = (int) (Math.random() * this.proveedores.length);
        Proveedor prov = this.proveedores[idProv];
        ColaP<Persona> personas = m.getPersonas();
        Pila<Vacuna> vacunas = prov.getVacunas();

        while (!personas.esVacia() && !vacunas.isVacia()) {

            Persona p = personas.deColar();
            Vacuna v = vacunas.pop();
            fecha = this.getFecha(fecha);

            if (p.getEdad() > edadEtapa && fecha.compareTo(v.getFecha_expiracion()) < 0) {
                NotificacionVacunado noti = new NotificacionVacunado(fecha, p, v);
                registros.insertarFin(noti);
                notificaciones++;
                fecha = fecha.plusMinutes(20);
            } else {
                personas.enColar(p, p.getPrioridad());
                vacunas.push(v);
                break;
            }
        }
        return notificaciones;
    }

    private LocalDateTime getFecha(LocalDateTime fecha) {
        if (fecha.getHour() == 12) {
            fecha = LocalDateTime.of(fecha.getYear(), fecha.getMonth(), fecha.getDayOfMonth(), 14, 0);
        }
        if (fecha.getHour() == 18) {
            fecha = LocalDateTime.of(fecha.getYear(), fecha.getMonth(), fecha.getDayOfMonth()+1, 6, 0);
        }
        if (fecha.getDayOfWeek().getValue() == 6) {
            fecha = fecha.plusDays(2);
        }
        return fecha;
    }

    public int getEtapa() {
        return etapa;
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public ListaCD<NotificacionVacunado> getRegistros() {
        return registros;
    }

    public Proveedor[] getProveedores() {
        return proveedores;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    public void setUrlDpto(String urlDpto) {
        this.urlDpto = urlDpto;
    }

    public void setUrlMunicipio(String urlMunicipio) {
        this.urlMunicipio = urlMunicipio;
    }

    public void setUrlPersona(String urlPersona) {
        this.urlPersona = urlPersona;
    }

    public void setUrlContenedor(String urlContenedor) {
        this.urlContenedor = urlContenedor;
    }

    public void setProveedores(Proveedor[] proveedores) {
        this.proveedores = proveedores;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Departamento d : this.dptos) {
            msg += d.toString() + "\n";
        }
        return msg;
    }

}
