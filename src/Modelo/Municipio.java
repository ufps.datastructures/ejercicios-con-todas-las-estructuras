package Modelo;

import Util.ColaP;
import java.time.LocalDateTime;

public class Municipio implements Comparable{

    private int id_municipio;

    private String nombre;
    
    private ColaP<Persona> personas = new ColaP();
    

    public Municipio() {
        
    }

    public Municipio(int id_municipio, String nombre) {
        this.id_municipio = id_municipio;
        this.nombre = nombre;
    }

    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ColaP<Persona> getPersonas() {
        return this.personas;
    }

    public void setPersonas(ColaP<Persona> personas) {
        this.personas = personas;
    }

    public void addPersona(long cedula, String nombre, LocalDateTime fecha, String email){
        Persona p = new Persona(cedula,nombre,fecha,email);
        this.personas.enColar(p, p.getPrioridad());
    }

    @Override
    public String toString() {
        String msg = this.nombre + "\n";
        ColaP<Persona> temp = new ColaP();
        while(!this.personas.esVacia()){
            Persona p = this.personas.deColar();
            temp.enColar(p, p.getPrioridad());
            msg += p.toString() + "\n";
        }
        this.personas = temp;
        return msg;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    
    
}
