/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.LocalDateTime;

/**
 *
 * @author madarme
 */
public class NotificacionVacunado implements Comparable {

    private LocalDateTime fechaVacunar;
    private Persona persona_vacunar;
    private Vacuna vacuna_asignada;

    public NotificacionVacunado() {
    }

    public NotificacionVacunado(LocalDateTime fechaVacunar, Persona persona_vacunar, Vacuna vacuna_asignada) {
        this.fechaVacunar = fechaVacunar;
        this.persona_vacunar = persona_vacunar;
        this.vacuna_asignada = vacuna_asignada;
    }

    public LocalDateTime getFechaVacunar() {
        return fechaVacunar;
    }

    public void setFechaVacunar(LocalDateTime fechaVacunar) {
        this.fechaVacunar = fechaVacunar;
    }

    public Persona getPersona_vacunar() {
        return persona_vacunar;
    }

    public void setPersona_vacunar(Persona persona_vacunar) {
        this.persona_vacunar = persona_vacunar;
    }

    public Vacuna getVacuna_asignada() {
        return vacuna_asignada;
    }

    public void setVacuna_asignada(Vacuna vacuna_asignada) {
        this.vacuna_asignada = vacuna_asignada;
    }

    @Override
    public String toString() {
        String msg = "Estimado " + this.persona_vacunar.getNombre() + ", se le informa que su vacunacion sera en la siguiente fecha: " + this.fechaVacunar.toString();
        return msg;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

}
