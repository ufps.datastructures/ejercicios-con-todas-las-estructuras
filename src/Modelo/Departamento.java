package Modelo;

import Util.ListaCD;

public class Departamento implements Comparable{

    private int id_dpto;
    private String nombreDpto;
    private ListaCD<Municipio> municipios=new ListaCD();

    public Departamento() {
        
    }

    public Departamento(int id_dpto, String nombreDpto) {
        this.id_dpto = id_dpto;
        this.nombreDpto = nombreDpto;
    }

    public int getId_dpto() {
        return id_dpto;
    }

    public void setId_dpto(int id_dpto) {
        this.id_dpto = id_dpto;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public ListaCD<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(ListaCD<Municipio> municipios) {
        this.municipios = municipios;
    }
    
    public void addMunicipio(int id, String name){
        this.municipios.insertarFin(new Municipio(id,name));
    }
    
    public Municipio getMunicipio(int id){
        for(Municipio m : this.municipios){
            if(m.getId_municipio() == id){
                return m;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String msg = "Departamento: " + this.nombreDpto + "\n";
        for(Municipio m : this.municipios){
            msg += m.toString();
        }
        return msg;
    }
    
    @Override
    public int compareTo(Object o) {
        return 0;
    }
    
    
    
}
