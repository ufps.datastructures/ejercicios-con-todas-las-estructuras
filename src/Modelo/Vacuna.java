/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.LocalDateTime;

/**
 *
 * @author madarme
 */
public class Vacuna implements Comparable{
    
    private int id_vacuna;
    private LocalDateTime fecha_expiracion;

    public Vacuna() {
    }

    public Vacuna(int id_vacuna, LocalDateTime fecha_expiracion) {
        this.id_vacuna = id_vacuna;
        this.fecha_expiracion = fecha_expiracion;
    }

    public int getId_vacuna() {
        return id_vacuna;
    }

    public void setId_vacuna(int id_vacuna) {
        this.id_vacuna = id_vacuna;
    }

    public LocalDateTime getFecha_expiracion() {
        return fecha_expiracion;
    }

    public void setFecha_expiracion(LocalDateTime fecha_expiracion) {
        this.fecha_expiracion = fecha_expiracion;
    }

    @Override
    public String toString() {
        return "Vacuna{" + "id_vacuna=" + id_vacuna + ", fecha_expiracion=" + fecha_expiracion + '}';
    }

    @Override
    public int compareTo(Object o) {
       return 0;
    }
    
    
    
    
}
