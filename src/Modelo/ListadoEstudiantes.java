/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author profesor
 */
public class ListadoEstudiantes {

    private Estudiante[] estudiantes;

    public ListadoEstudiantes() {
    }

    public ListadoEstudiantes(Estudiante[] estudiantes) {
        this.estudiantes = estudiantes;
    }

    public Estudiante[] getEstudiantes() {
        return estudiantes;
    }

    public void setEstudiantes(Estudiante[] estudiantes) {
        this.estudiantes = estudiantes;
    }

    @Override
    public String toString() {

        String msg = "";
        for (Estudiante estudiante : estudiantes) {
            msg += estudiante.toString() + "\n";

        }
        return msg;
    }

    public void sort()
    {
        
        Estudiante ArrayN[]=this.estudiantes;
    /* Bucle desde 0 hasta la longitud del array -1 */
        for (int i = 0; i < ArrayN.length - 1; i++) {
            /* Bucle anidado desde 0 hasta la longitud del array -1 */
            for (int j = 0; j < ArrayN.length - 1; j++) { /* Si el número almacenado en la posición j es mayor que el de la posición j+1 (el siguiente del array) */ 
                
                int c=ArrayN[j].compareTo(ArrayN[j + 1]);
                if (c>0) {
                    /* guardamos el número de la posicion j+1 en una variable (el menor) */
                    Estudiante temp = ArrayN[j + 1];
                    /* Lo intercambiamos de posición */
                    ArrayN[j + 1] = ArrayN[j];
                    ArrayN[j] = temp;
                    /* y volvemos al inicio para comparar los siguientes hasta que todos se hayan comparado*/
                    /* de esta forma vamos dejando los números mayores al final del array en orden*/
                }
            }
        }
    }
    
    
}
