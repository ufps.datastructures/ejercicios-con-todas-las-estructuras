/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.ListaCD;

/**
 *
 * @author profesor
 */
public class Semestre {
    
    private byte id;
    private ListaCD<Estudiante> estudiantes = new ListaCD();
    
    public Semestre(){
        
    }
    
    public Semestre(byte id){
        this.id = id;
    }
    
    public void addEstudiante(String datos){
        String [] datosEst = datos.split(";");
        long codigo = Long.parseLong(datosEst[1]);
        String nombre = datosEst[2];
        float p1 = Float.parseFloat(datosEst[3]);
        float p2 = Float.parseFloat(datosEst[4]);
        float p3 = Float.parseFloat(datosEst[5]);
        float examen = Float.parseFloat(datosEst[6]);
        this.estudiantes.insertarFin(new Estudiante(nombre, codigo, p1, p2, p3, examen));
    }
    
    public ListaCD<String> getListado(){
        ListaCD<String> listado = new ListaCD<String>();
        Estudiante e;
        for(int i = 0; i < this.estudiantes.getSize(); i++){
            e = this.estudiantes.get(i);
            String info = e.getCodigo() + " - " + e.getNombre() + " - " + e.getPromedio();
            listado.insertarFin(info);
        }
        return listado;
    }
}
