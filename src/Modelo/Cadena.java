/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.Cola;
import Util.Pila;

/**
 *
 * @author docente
 */
public class Cadena {

    private char letras[];

    public Cadena() {
    }

    public Cadena(String letras) {
        this.letras = letras.toCharArray();
    }

    public String toString() {
        String m = "";
        for (int i = 0; i < letras.length; i++) {
            m += letras[i] + "\t";
        }
        return m;
    }

    public boolean isPalindrome() {
        if (this.letras == null) {
            throw new RuntimeException("Cadena vacía");
        }

        Cola<Character> cola = new Cola();
        Pila<Character> pila = new Pila();
        for (int i = 0; i < letras.length; i++) {
            cola.enColar(letras[i]);
            pila.push(letras[i]);
        }
        return isIgual(cola, pila);
    }

    private boolean isIgual(Cola<Character> c, Pila<Character> p) {
        if (c == null || p == null || c.isVacia() || p.isVacia()) {
            throw new RuntimeException("No puedo comparar");
        }

        if (c.size() != p.size()) {
            return false;
        }
        //Iterar:
        while (!c.isVacia()) {
            char c1 = c.deColar();
            char c2 = p.pop();
            if (c1 != c2) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * No se puede usar el size. La cadena sólo puede tener a y b
     * Ejemp: aab -> True
     * Ejem: aabn --> False
     * Ejem: aabbaaababaa--> True
     * @return true si la cadena contiene el doble de "a" que de "b"
     */
    public boolean isPar()
    {
        return false;
    }
    
    
    /**
     * No se puede usar el size. La cadena sólo puede tener a y b
     * Ejemp: aab -> True
     * Ejem: aabn --> False
     * Ejem: aabbaaababaa--> False
     * Ejem: aaaaaaaabbbb --> True
     * Ejem: aabaab --> True
     * @return true si la cadena contiene el doble de "a" que de "b" en orden 
     */
    public boolean isPar2()
    {
        return false;
    }
    
    
}
